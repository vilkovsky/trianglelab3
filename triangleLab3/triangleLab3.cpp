﻿#include "pch.h"
#include <iostream>
#include "MySettings.h"
#include "Triangle.h"
#include <conio.h>

void menu();

int main()
{
	MySettings mySettings;

	menu();
	
//	_getch();
}



void menu() {
	char key;

	Triangle triangle;
	RectangularTriangle rectangularTriangle;

	do {
		cout << "----------------------------------" << endl;
		cout << "\t1 - Ввод параметров треугольника (длины сторон)" << endl;
		cout << "\t2 - Расчет периметра" << endl;
		cout << "\t3 - Расчет площади" << endl;
		cout << "\t4 - Ввод параметров для прямоугольного треугольника" << endl;
		cout << "\t5 - Расчет площади для прямоугольного треугольника" << endl;
		cout << "\tESC - выход" << endl;
		cout << "\tВаш выбор: ";
		cin >> key;

		switch (key)
		{
		case '1':
			triangle.input();
			break;
		case '2':
			triangle.findPerimeter();
			break;
		case '3':
			triangle.findSquare();
			break;
		case '4':
			rectangularTriangle.input();
			break;
		case '5':
			rectangularTriangle.findSquare();
			break;
		default:
			cout << "Дана не управляющая команда!" << endl;
		}

	} while (true);
}
