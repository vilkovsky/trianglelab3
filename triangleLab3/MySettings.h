#pragma once
#include <string>

using namespace std;

class MySettings
{
private:
	const int NumLab = 3;
	const string Task = "������� ������� ����� ������������, ������ � ��� ������� ������� ���������� ������������, ������ �� �� �����, ���������� ��������� � �������." 
		"�������� �� ���� ����� �������������� �����������, ������������� ������� ������� ���������� � ���������� �������. ����������� �������� ������� ������. "
		"��� ����� ���������� ����������� ���������� �������� �� ������������� ������������. "
		"��� ������� ���������� ��� ������� ������ �������������� ����������� ���������, ������������� �� ����� ������������� �����������.";
	const int Semester = 2;
	const int Course = 1;
	const int Variant = 1;
	const string Executor = "Vilkovsky Oleg Sergeevich";
	const string Email = "olegvilkovsky@gmail.com";	
	const string Group = "���-18�";
	void cap();
		
public:
	MySettings();
	~MySettings();
};

