#pragma once
class Triangle
{
private:
	double a, b, c;

protected:
	void checkTriangle();

public:
	double getA() {
		return a;
	}

	void setA(double value) {
		a = value;
	}

	double getB() {
		return b;
	}

	void setB(double value) {
		b = value;
	}

	double getC() {
		return c;
	}

	void setC(double value) {
		c = value;
	}

	Triangle();
	~Triangle();

	virtual void input();
	void findPerimeter();
	virtual void findSquare();
};

class RectangularTriangle: private Triangle
{
public:
	virtual void input();
	virtual void findSquare();
};

