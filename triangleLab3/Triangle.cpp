#include "pch.h"
#include "Triangle.h"
#include <iostream>

using namespace std;

double inputOnlyNumber();
double checkLength();

Triangle::Triangle()
{
	a = 0;
	b = 0;
	c = 0;
}

Triangle::~Triangle()
{
}

void Triangle::input() {
	cout << "������� ������� a: ";
	a = checkLength();

	cout << "������� ������� b: ";
	b = checkLength();

	cout << "������� ������� c: ";
	c = checkLength();

	checkTriangle();
}

void RectangularTriangle::input() {
	cout << "������� ����� a: ";
	setA(checkLength());

	cout << "������� ����� b: ";
	setB(checkLength());

	setC(sqrt(getA()*getA() + getB() * getB()));
	cout << "����� ����������: " << getC() << endl;

	checkTriangle();
}

void Triangle::findPerimeter() {
	double perimeter = a + b + c;
	cout << "�������� ������������: " << perimeter << endl;
}

void Triangle::findSquare() {
	double p, s;
	p = (a + b + c) / 2.0;
	// ������� ������
	s = sqrt(p * (p - a) * (p - b) * (p - c));
	cout << "������� ������������: " << s << endl;
}

void RectangularTriangle::findSquare() {
	double s = getA() * getB() / 2;
	cout << "������� �������������� ������������: " << s << endl;
}

double checkLength() {
	double var;
	do {
		var = inputOnlyNumber();
	} while (var <= 0);
	return var;
}


double inputOnlyNumber() {
	double var;
	while (!(cin >> var) || cin.peek() != '\n')
	{
		cin.clear();
		while (cin.get() != '\n');
		cout << "Error! Retry input\n";
		cout << "Enter the number: " << "\n";

	}
	return var;
}

void Triangle::checkTriangle() {
	if (a + b > c && a + c > b && b + c > a) {
		cout << "����������� ����������" << endl;
	}
	else {
		cout << "����������� �� ����������, ���������� ���������" << endl;
		a = 0;
		b = 0;
		c = 0;
	}
}